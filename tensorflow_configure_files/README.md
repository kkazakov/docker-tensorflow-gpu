# Those are the files that tensorflow was built with. To use them:


```bash
cd ~/
git clone https://github.com/tensorflow/tensorflow.git
cd tensorflow
git pull
git checkout r1.8
```

Copy the two files in the folder or run ./configure

To build Tensorflow, run:

```bash
bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package
```

To create package, run:

```bash
bazel-bin/tensorflow/tools/pip_package/build_pip_package tensorflow_pkg
```


Note: If the package build is for Python2, make sure that your default Python is 3!!! Bazel has a bug that selects python2 at some point.


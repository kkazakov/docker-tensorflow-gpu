
Tensorflow wheels built for older Xeon processors ( Mine are E5620s on HP Workstation Z600 ) - 1.8, 1.9rc0

Docker files for Ubuntu 18.04, Cuda 9.2, Tensorflow, Keras and more. Runs great with GPU.

Make sure you have nvidia-docker installed on host machine.
For Ubuntu, I recommend reading http://www.python36.com/how-to-install-tensorflow-gpu-with-cuda-9-2-for-python-on-ubuntu/


